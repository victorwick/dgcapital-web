import { Button as ButtonLib } from "antd";
import styled, { css } from "styled-components";
import colors from "../../configs/colors";

const Button = styled(ButtonLib)`
  border-radius: ${(props) => {
    return props.rounded ? "40px" : "6px";
  }};
  padding: 0 24px;
  height: 56px;
  font-size: 16px;
  font-weight: 600;

  ${(props) => {
    return (
      props.type === "primary" &&
      props.className === "transparent" &&
      css`
        background: transparent;
        color: ${colors.teal500};
      `
    );
  }}

  ${(props) => {
    return (
      props.type === "secondary" &&
      props.className === "transparent" &&
      css`
        background: transparent;
        color: ${colors.white900};
      `
    );
  }}

${(props) => {
    return (
      props.size === "small" &&
      css`
        height: 40px;
        padding: 0 16px;
        min-width: 120px;
        font-size: 12px;
        font-weight: 600;
      `
    );
  }}
`;

export default Button;
