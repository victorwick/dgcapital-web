import React from "react";
import Wrapper from "./style";

const Index = (props) => {
  const { children } = props;
  return <Wrapper>{children}</Wrapper>;
};

export default Index;
