import styled from "styled-components";

const Wrapper = styled.div`
  max-width: calc(1128px + 40px);
  padding: 0 40px;
  width: 100%;
`;

export default Wrapper;
