import { Divider } from "antd";
import React from "react";
import Wrapper from "./style";
import BASE_URL from "../../configs/baseUrl";
import { IconDGVLogoWhite } from "../../assets";
import Image from "next/image";
import { useRouter } from "next/router";

const menus = [
  {
    to: BASE_URL.ABOUT_US,
    label: "About Us",
  },
  {
    to: BASE_URL.OUR_PORTFOLIO,
    label: "Our Portfolio",
  },
  {
    to: BASE_URL.PUBLICATION,
    label: "Publication",
  },
  {
    to: BASE_URL.CONTACT_US,
    label: "Contact Us",
  },
];

const Index = () => {
  const router = useRouter();

  const onChangePageHandler = (page) => {
    router.push({
      pathname: BASE_URL.LANDING,
      query: { active_section: page },
    });
  };

  return (
    <Wrapper>
      <div className="footer_wrapper">
        <div className="component_footer_top">
          <div className="component_base">
            <div className="component_base_logo">
              <Image src={IconDGVLogoWhite} alt="dgv logo" />
            </div>
            <div className="component_base_address">
              Jl. Senopati No 89 Kebayoran Lama, Jakarta Selatan 12190.
            </div>
          </div>
          <div className="component_nav_list">
            {menus?.map((item, index) => {
              return (
                <div
                  key={item?.to}
                  className="component_nav_list_item"
                  onClick={() => {
                    onChangePageHandler(index + 1);
                  }}
                >
                  {item?.label}
                </div>
              );
            })}
          </div>
        </div>
        <Divider style={{ margin: 0 }} />
        <div className="component_footer_bottom">
          @ DG Capital Team 2021, all right reserved
        </div>
      </div>
    </Wrapper>
  );
};

export default Index;
