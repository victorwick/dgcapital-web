import styled from "styled-components";
import breakpoints from "../../configs/breakpoints";
import colors from "../../configs/colors";

const Wrapper = styled.div`
  background: ${colors.gradientCoal};
  width: 100%;
  margin-top: 80px;
  .footer_wrapper {
    max-width: calc(1128px);
    margin: 0 auto;
    .component_footer_top {
      width: 100%;
      padding: 40px;
      display: grid;
      align-items: flex-start;
      grid-template-columns: 1fr auto;
      gap: 20px;
      .component_base {
        .component_base_address {
          font-size: 14px;
          font-weight: 400;
          line-height: 20px;
          color: ${colors.white900};
          margin-top: 10px;
        }
      }
      .component_nav_list {
        display: flex;
        justify-content: end;
        gap: 24px;
        .component_nav_list_item {
          padding: 6px 12px;
          cursor: pointer;
          transition: 0.3s ease-in-out;
          font-size: 16px;
          font-weight: 700;
          line-height: 24px;
          color: ${colors.white900};
          :hover {
            color: ${colors.teal500};
          }
        }
      }
    }

    .ant-divider {
      background: ${colors.gray400};
    }

    .component_footer_bottom {
      width: 100%;
      padding: 12px 40px;
      font-size: 14px;
      font-weight: 400;
      line-height: 20px;
      color: ${colors.gray400};
    }
  }

  @media screen and (max-width: ${breakpoints.tab_large}) {
    .footer_wrapper {
      .component_footer_top {
        .component_nav_list {
          gap: 16px;
          .component_nav_list_item {
            padding: 6px 8px;
          }
        }
      }
    }
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    .footer_wrapper {
      .component_footer_top {
        grid-template-columns: 1fr;
        gap: 24px;
        justify-content: center;
        align-items: center;
        .component_base {
          text-align: center;
        }
        .component_nav_list {
          justify-content: center;
          flex-wrap: wrap;
        }
      }
      .component_footer_bottom {
        text-align: center;
      }
    }
  }
`;

export default Wrapper;
