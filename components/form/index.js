import styled from "styled-components";
import { Form as FormAntd } from "antd";
import color from "../../configs/colors";
import colors from "../../configs/colors";
import thumbStyle from "../../configs/thumbStyle";

const Form = styled(FormAntd)`
  width: 100%;
  .ant-form-item-control-input-content .ant-input,
  .ant-input-affix-wrapper.ant-input-password,
  textarea,
  .ant-picker,
  .ant-input-number {
    border: 1px solid
      ${(props) => {
        return props.theme === "dark" ? "#3F3F46" : "#D1D5DB";
      }} !important;
    border-radius: ${(props) => {
      return props.rounded ? "32px" : "8px";
    }};
  }

  .ant-form-item-has-error
    :not(.ant-input-disabled):not(.ant-input-borderless).ant-input,
  .ant-form-item-has-error
    :not(.ant-input-disabled):not(.ant-input-borderless).ant-input:hover {
    display: none !important;
  }

  .ant-form-item-label
    > label.ant-form-item-required:not(.ant-form-item-required-mark-optional) {
    ::before {
      display: none;
    }
    ::after {
      display: inline-block;
      margin-left: 4px;
      color: ${colors.gray400};
      font-size: 14px;
      font-family: SimSun, sans-serif;
      line-height: 1;
      content: "*";
    }
  }

  .ant-input {
    font-size: 16px;
    min-height: 56px;
    padding: 0 24px;
    background: ${(props) => {
      return props.theme === "dark" ? colors.gray800 : colors.white900;
    }};
    color: ${(props) => {
      return props.theme === "dark" ? colors.white900 : colors.gray900;
    }};
    ::placeholder {
      color: ${(props) => {
        return props.theme === "dark" ? colors.gray600 : "#D0D0D0";
      }};
    }
  }
  textarea.ant-input {
    padding: 16px 24px;
    ${thumbStyle};
  }

  .ant-form-item {
    margin-bottom: 16px;
  }

  .ant-form-item-label > label {
    font-size: 14px;
    font-weight: 600;
    color: ${(props) => {
      return props.theme === "dark" ? colors.gray400 : colors.gray900;
    }};
  }

  .ant-picker-input > input {
    font-size: 16px;
  }

  .ant-select-disabled.ant-select:not(.ant-select-customize-input)
    .ant-select-selector {
    color: ${color.black2};
  }

  .ant-form-item-control {
    .ant-checkbox-group {
      &.vertical {
        display: flex;
        flex-direction: column;
        gap: 12px;
      }
    }
  }

  .ant-form-item-explain {
    color: #ff4d4f;
  }

  .ant-checkbox-wrapper {
    color: ${(props) => {
      return props.theme === "dark" ? colors.gray300 : colors.gray900;
    }};
    font-size: 14px;
    font-weight: 400;
    .ant-checkbox-inner {
      height: 18px;
      width: 18px;
      border: 2px solid
        ${(props) => {
          return props.theme === "dark" ? "#DADADA" : "unset";
        }};
      background-color: ${(props) => {
        return props.theme === "dark" ? "transparent" : "white";
      }};
    }
  }

  .ant-form-item-explain-error {
    margin-top: 6px;
  }

  .ant-input-affix-wrapper {
    font-size: 16px;
    padding: 0 24px;
    background: ${(props) => {
      return props.theme === "dark" ? colors.gray800 : colors.white900;
    }};
    color: red;
    > .ant-input {
      border: none !important;
      min-height: 54px;
    }
    > .ant-input-suffix .anticon {
      color: ${(props) => {
        return props.theme === "dark" ? colors.white900 : colors.gray600;
      }};
    }
  }
`;

export default Form;
