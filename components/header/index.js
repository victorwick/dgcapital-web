import React, { useState } from "react";
import Image from "next/image";
import BASE_URL from "../../configs/baseUrl";
import Wrapper, { Menus } from "./style";
import { IconClose, IconDGVLogoPrimary, IconMenu } from "../../assets";
import { Col, Row, Tabs } from "antd";

const menus = [
  {
    to: BASE_URL.LANDING,
    label: "Home",
  },
  {
    to: BASE_URL.ABOUT_US,
    label: "About Us",
  },
  {
    to: BASE_URL.OUR_PORTFOLIO,
    label: "Our Portfolio",
  },
  {
    to: BASE_URL.PUBLICATION,
    label: "Publication",
  },
  {
    to: BASE_URL.CONTACT_US,
    label: "Contact Us",
  },
];

const Index = (props) => {
  const { currentPage, onChangePageHandler } = props;
  const [showDrawer, setShowDrawer] = useState(false);
  return (
    <>
      <Wrapper>
        <div className="header_wrapper">
          <div className="component_logo">
            <Image src={IconDGVLogoPrimary} alt="dgv" />
          </div>
          <div className="component_nav">
            <Tabs
              activeKey={currentPage?.toString()}
              onChange={(value) => {
                onChangePageHandler(Number(value));
              }}
            >
              {menus.map((item, index) => {
                return <Tabs.TabPane tab={item?.label} key={Number(index)} />;
              })}
            </Tabs>
            <div className="component_nav_menu">
              <div
                aria-hidden="true"
                onClick={() => {
                  setShowDrawer(true);
                }}
                className="component_nav_menu_btn"
              >
                <IconMenu />
              </div>
            </div>
          </div>
        </div>
      </Wrapper>
      <Menus
        placement="right"
        closable={false}
        title={(() => {
          return (
            <Row justify="space-between" align="middle">
              <Col>
                <Image src={IconDGVLogoPrimary} alt="dgv" />
              </Col>
              <Col
                className="component_menu_close"
                onClick={() => {
                  setShowDrawer(false);
                }}
              >
                <IconClose />
              </Col>
            </Row>
          );
        })()}
        visible={showDrawer}
        onClose={() => {
          setShowDrawer(false);
        }}
      >
        <div className="component_menu_list">
          {menus.map((item, index) => {
            const isActive = currentPage === index;
            return (
              <div
                key={item?.to}
                className={`component_menu_list_item ${isActive && "active"}`}
                onClick={() => {
                  setShowDrawer(false);
                  onChangePageHandler(index);
                }}
              >
                {item?.label}
              </div>
            );
          })}
        </div>
      </Menus>
    </>
  );
};

export default Index;
