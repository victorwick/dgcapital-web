import { Drawer } from "antd";
import styled from "styled-components";
import breakpoints from "../../configs/breakpoints";
import colors from "../../configs/colors";

const Wrapper = styled.div`
  height: 76px;
  position: fixed;
  width: 100%;
  z-index: 999;
  top: 0;
  .header_wrapper {
    padding: 0 40px;
    max-width: calc(1128px + 40px);
    height: 100%;
    margin: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .component_nav {
    height: inherit;
    .ant-tabs {
      height: 100%;
      display: grid;
      .ant-tabs-nav {
        margin: 0;
        ::before {
          display: none;
        }
        .ant-tabs-tab {
          :hover {
            color: ${colors.teal500};
          }
          .ant-tabs-tab-btn {
            font-size: 16px;
            font-weight: 700;
            color: ${colors.gray400};
            :hover {
              color: ${colors.teal500};
            }
            :focus {
              color: ${colors.gray400};
            }
          }
          &.ant-tabs-tab-active {
            .ant-tabs-tab-btn {
              color: ${colors.teal500};
            }
          }
        }
        .ant-tabs-ink-bar {
          background-color: ${colors.teal500};
          height: 1px;
        }
      }
      .ant-tabs-content-holder {
        display: none;
      }
    }

    .component_nav_menu {
      margin: auto;
      height: 100%;
      display: none;
      align-items: center;
      justify-content: center;
      .component_nav_menu_btn {
        padding: 4px;
        svg {
          cursor: pointer;
          fill: ${colors.gray400};
          transition: 300ms ease-in-out;
          :hover {
            fill: ${colors.teal500};
          }
        }
      }
    }
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    .component_nav {
      .ant-tabs {
        display: none;
      }
      .component_nav_menu {
        display: flex;
      }
    }
  }
`;

export default Wrapper;

export const Menus = styled(Drawer)`
  display: none;
  @media screen and (max-width: ${breakpoints.tab}) {
    display: block;
  }
  .component_menu_close {
    cursor: pointer;
    svg {
      fill: ${colors.gray400};
      transition: 300ms ease-in-out;
      :hover {
        fill: ${colors.teal500};
      }
    }
  }

  .component_menu_list {
    display: grid;
    gap: 32px;
    .component_menu_list_item {
      font-size: 16px;
      font-weight: 700;
      color: ${colors.gray400};
      padding: 12px 0;
      cursor: pointer;
      :hover {
        color: ${colors.teal500};
      }
      &.active {
        color: ${colors.teal500};
      }
    }
  }
`;
