import React from "react";

const Index = (props) => {
  const { iframe } = props;
  return (
    <div
      className="component_iframe"
      dangerouslySetInnerHTML={{ __html: iframe }}
    />
  );
};

export default Index;
