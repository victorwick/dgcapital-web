import React from "react";
import Wrapper from "./style";

const Index = (props) => {
  const {
    title = "Page Header Here",
    dark_text,
    margin_top = false,
    full_width,
  } = props;
  return (
    <Wrapper
      dark_text={dark_text}
      margin_top={margin_top}
      full_width={full_width}
    >
      <div className="component_page_header">{title}</div>
    </Wrapper>
  );
};

export default Index;
