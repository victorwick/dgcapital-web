import styled from "styled-components";
import colors from "../../configs/colors";

const Wrapper = styled.div`
  margin-top: ${({ margin_top }) => {
    return margin_top ? `${margin_top}px` : 0;
  }};
  .component_page_header {
    display: flex;
    align-items: center;
    font-size: 18px;
    font-weight: 700;
    color: ${(props) => {
      return props.dark_text ? colors.white900 : colors.gray900;
    }};
    margin: 0 auto;
    margin-bottom: 40px;
    max-width: ${(props) => {
      return props.full_width ? "100%" : "940px";
    }};
    ::after {
      content: "";
      margin-left: 8px;
      height: 1px;
      width: 128px;
      background-color: ${(props) => {
        return props.dark_text ? colors.white900 : colors.gray900;
      }};
    }
  }
`;

export default Wrapper;
