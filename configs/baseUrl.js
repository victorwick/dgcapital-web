export default {
  LANDING: "/",
  ABOUT_US: "/about-us",
  OUR_PORTFOLIO: "/our-portfolio",
  PUBLICATION: "/publication",
  CONTACT_US: "/contact-us",
};
