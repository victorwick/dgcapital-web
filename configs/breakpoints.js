export default {
  desktop: "1280px",
  phone: "575px",
  tab: "768px",
  tab_large: "992px",
};
