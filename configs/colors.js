export default {
  // TEAL
  teal500: "#14B8A6",

  // GRAY
  gray900: "#111827",
  gray800: "#27272A",
  gray600: "#4B5563",
  gray500: "#F9FAFB",
  gray400: "#9CA3AF",
  gray300: "#D1D5DB",
  gray200: "#E5E7EB",
  gray100: "#F3F4F6",

  // WHITE
  white900: "#FFFFFF",

  // GRADIENT
  gradientCoal: "linear-gradient(135deg, #374151 0%, #111827 99.48%)",
  gradientDark: "linear-gradient(135deg, #3C3B3B 0%, #0B0907 99.48%)",
  gradientWaves: "linear-gradient(135deg, #3C3B3B 0%, #0B0907 99.48%)",
  gradientTeal: "linear-gradient(270deg, #15927C 0%, #20C997 100%)",
};
