import { css } from "styled-components";
import colors from "./colors";

const thumbStyle = css`
  &::-webkit-scrollbar {
    background: transparent;
    height: 6px;
    width: 6px;
    border-radius: 12px;
    position: absolute;
  }
  &::-webkit-scrollbar-thumb {
    border-radius: 100px;
    background: ${colors.gray400};
  }
`;

export default thumbStyle;
