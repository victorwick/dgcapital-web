import React from "react";
import Image from "next/image";
import { ImageBlockchain, ImageDotsPattern } from "../../assets";
import { AboutWrapper } from "./style";
import { ContentSection, PageHeader } from "../../components";

const AboutSection = (props) => {
  const { id } = props;
  return (
    <AboutWrapper className="section about" id={`section-${id}`}>
      <ContentSection>
        <PageHeader title="About Us" />
        <div className="component_body">
          <div className="component_body_text">
            <div className="component_title">
              Researching Blue Chips in the cryptoverse is our speciality
            </div>
            <div className="component_description">
              We aim to be the leading regional capital in money technology
              blockchain and set to achieve our goal with fast Indonesia's money
              inclusion and digital economy society by empowering the start-up
              scheme.
            </div>
          </div>
          <div className="component_body_image">
            <Image src={ImageBlockchain} alt="digital currency" />
          </div>
        </div>
      </ContentSection>
      <div className="component_about_bg">
        <Image src={ImageDotsPattern} alt="dots pattern" />
      </div>
    </AboutWrapper>
  );
};

export default AboutSection;
