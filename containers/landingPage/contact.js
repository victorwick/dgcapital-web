import { Input } from "antd";
import React from "react";
import {
  Button,
  ContentSection,
  Form,
  Iframe,
  PageHeader,
} from "../../components";
import { ContactWrapper } from "./style";

const ContactSection = (props) => {
  const { id } = props;

  const iframe =
    '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.255391176331!2d106.80391901537224!3d-6.230023395490237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f15ad0e3752d%3A0xb26900759d1202!2sJl.%20Senopati%20No.89%2C%20RT.8%2FRW.3%2C%20Senayan%2C%20Kec.%20Kby.%20Baru%2C%20Kota%20Jakarta%20Selatan%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2012110!5e0!3m2!1sen!2sid!4v1641400118520!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>';

  return (
    <ContactWrapper className="section contact" id={`section-${id}`}>
      <ContentSection>
        <PageHeader title="Contact Us" dark_text />
        <div className="component_body">
          <div className="component_body_text">
            <div className="component_title">DG Capital Tower</div>
            <div className="component_desc">
              Jl. Senopati No 89 Kebayoran Lama, Jakarta Selatan 12190.
            </div>
            <div className="component_map">
              <Iframe iframe={iframe} />
            </div>
          </div>
          <div className="component_body_form">
            <Form theme="dark">
              <Form.Item name="name">
                <Input placeholder="Your Name" />
              </Form.Item>
              <Form.Item name="username">
                <Input placeholder="Your Telegram Username" />
              </Form.Item>
              <Form.Item name="email">
                <Input placeholder="Your Email" />
              </Form.Item>
              <Form.Item name="message">
                <Input.TextArea rows={4} placeholder="Your Message" />
              </Form.Item>
              <Button type="primary" block style={{ marginTop: 16 }}>
                Submit
              </Button>
            </Form>
          </div>
        </div>
      </ContentSection>
    </ContactWrapper>
  );
};

export default ContactSection;
