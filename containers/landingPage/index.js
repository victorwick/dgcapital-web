import React, { useEffect, useState } from "react";
import ReactPageScroller from "react-page-scroller";
import { Header } from "../../components";
import Wrapper from "./style";
import MainSection from "./main";
import AboutSection from "./about";
import OurPortfolioSection from "./ourPortfolio";
import PublicationSection from "./publication";
import ContactSection from "./contact";
import useWindowDimensions from "../../hooks/dimension";
import { useRouter, withRouter } from "next/router";

const contentList = [
  {
    key: 0,
    content: MainSection,
  },
  {
    key: 1,
    content: AboutSection,
  },
  {
    key: 2,
    content: OurPortfolioSection,
  },
  {
    key: 3,
    content: PublicationSection,
  },
  {
    key: 4,
    content: ContactSection,
  },
];

const getDimensions = (element) => {
  const { height } = element?.getBoundingClientRect();
  const { offsetTop } = element;
  const offsetBottom = offsetTop + height;
  return { height, offsetTop, offsetBottom };
};

const Index = () => {
  const router = useRouter();
  const initialPage = router?.query?.active_section || 0;
  const [currentPage, setCurrentPage] = useState(Number(initialPage));
  const { width, height } = useWindowDimensions();
  const tabScreen = width < 768 || height < 720;

  const onChangePageHandler = (number) => {
    if (tabScreen) {
      const el = document.getElementById(`section-${number}`);
      el.scrollIntoView({ behavior: "smooth", block: "start" });
    }
    setCurrentPage(number);
  };

  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;
      const activeSection = contentList?.find((item) => {
        const element = document.getElementById(`section-${item?.key}`);
        if (element) {
          const { offsetBottom, offsetTop } = getDimensions(element);
          return scrollPosition < offsetBottom;
        }
        return element;
      });
      setCurrentPage(activeSection?.key);
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });

  const renderContentList = () => {
    if (tabScreen) {
      return contentList.map((item) => {
        const { content: Content } = item;
        return (
          <Content
            key={item?.key}
            id={item?.key}
            onChangePageHandler={onChangePageHandler}
          />
        );
      });
    }
    return (
      <ReactPageScroller
        pageOnChange={onChangePageHandler}
        customPageNumber={currentPage}
        renderAllPagesOnFirstRender
      >
        {contentList.map((item) => {
          const { content: Content } = item;
          return (
            <Content
              key={item?.key}
              onChangePageHandler={onChangePageHandler}
            />
          );
        })}
      </ReactPageScroller>
    );
  };

  return (
    <Wrapper>
      <Header
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        onChangePageHandler={onChangePageHandler}
      />
      {renderContentList()}
    </Wrapper>
  );
};

export default withRouter(Index);
