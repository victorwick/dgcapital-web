import Image from "next/image";
import React from "react";
import {
  IconArrowDownCircle,
  ImageDigitalCurrency,
  ImageWavePattern,
} from "../../assets";
import { MainWrapper } from "./style";

const MainSection = (props) => {
  const { onChangePageHandler, id } = props;
  return (
    <MainWrapper className="section main" id={`section-${id}`}>
      <div className="container_main">
        <div className="component_main_text">
          <div className="component_header">
            Investing and Scaling
            <br />
            in high-tension Data
            <br />
            Analytics.
          </div>
          <div className="component_divider" />
          <div className="component_description">
            Accelerating Indonesia's monetary inclusion and digital economy
            society by empowering good and strategic analysis scheme.
          </div>
        </div>
        <div className="component_image">
          <Image src={ImageDigitalCurrency} alt="digital currency" />
        </div>
      </div>
      <div className="component_main_bg">
        <div className="component_main_bg_wrapper">
          <div className="component_bg">
            <Image
              src={ImageWavePattern}
              alt="wave pattern"
              layout="responsive"
            />
          </div>
          <div className="component_scroll_down">
            <div
              className="component_scroll_down_btn"
              onClick={() => {
                onChangePageHandler(1);
              }}
            >
              <IconArrowDownCircle />
              Scroll Down
            </div>
          </div>
        </div>
      </div>
    </MainWrapper>
  );
};

export default MainSection;
