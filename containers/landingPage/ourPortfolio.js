import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import {
  IconFileImage,
  ImageGeometricPattern,
  ImageLineCirclePattern,
} from "../../assets";
import { ContentSection, PageHeader, Button } from "../../components";
import BASE_URL from "../../configs/baseUrl";
import { PortfolioWrapper } from "./style";

const dummy = [
  {
    id: "1",
    name: "Card name 1",
    image_url:
      "https://images.pexels.com/photos/1622419/pexels-photo-1622419.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
  },
  {
    id: "2",
    name: "Card name 2",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  },
  {
    id: "3",
    name: "Card name 3",
  },
  {
    id: "4",
    name: "Card name 4",
  },
];

const OurPortfolioSection = (props) => {
  const { id } = props;
  const router = useRouter();
  return (
    <PortfolioWrapper className="section portfolio" id={`section-${id}`}>
      <ContentSection>
        <PageHeader title="Our Portfolio" />
        <div className="component_body">
          <div className="component_description">
            We measure experienced project teams who have intensive experience
            and skills in the cryptoverse . we tend to believe that in order to
            accelerate Indonesia's economic process it is imperative to bring
            strategic value and partnership to the far side of capital
            contribution.
          </div>
          <div className="component_card_list">
            {dummy?.map((item) => {
              return (
                <div key={item?.id} className="component_card_list_item">
                  <div className="component_card_image_wrapper">
                    {item?.image_url && (
                      <img src={item?.image_url} alt="card image" />
                    )}
                    {!item?.image_url && <IconFileImage />}
                  </div>
                  <div className="component_card_text_wrapper">
                    {item?.name}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="component_cta">
          <Button
            type="primary"
            onClick={() => {
              router.push(BASE_URL.OUR_PORTFOLIO);
            }}
          >
            View Full Portfolio
          </Button>
        </div>
        <div className="component_about_bg left">
          <Image src={ImageGeometricPattern} alt="geometric pattern" />
        </div>
        <div className="component_about_bg right">
          <Image
            src={ImageLineCirclePattern}
            alt="line circle pattern"
            className="component_about_bg_right"
          />
        </div>
      </ContentSection>
    </PortfolioWrapper>
  );
};

export default OurPortfolioSection;
