import React, { useRef } from "react";
import { useRouter } from "next/router";
import SwiperCore, { Pagination, Navigation } from "swiper";
import { Button, ContentSection, PageHeader, Swiper } from "../../components";
import { PublicationWrapper } from "./style";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { IconArrowDownCircle, IconCalender, IconFileImage } from "../../assets";
import useWindowDimensions from "../../hooks/dimension";
import BASE_URL from "../../configs/baseUrl";

SwiperCore.use([Pagination]);
SwiperCore.use([Navigation]);

const dummy = [
  {
    id: "1",
    title: "#1 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  },
  {
    id: "2",
    title: "#2 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
  },
  {
    id: "3",
    title: "#3 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
  },
  {
    id: "4",
    title: "#4 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
  },
  {
    id: "5",
    title: "#5 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  },
  {
    id: "6",
    title: "#6 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  },
];

const PublicationSection = (props) => {
  const { id } = props;
  const router = useRouter();
  const prevSlideBtn = useRef(null);
  const nextSlideBtn = useRef(null);

  const { width } = useWindowDimensions();
  const tabLargeScreen = width < 992;
  const phoneScreen = width < 575;

  const onGetSwiperHandler = () => {
    if (phoneScreen) {
      return {
        slidesPerView: 1,
      };
    }
    if (tabLargeScreen) {
      return {
        slidesPerView: 2,
      };
    }
    return {
      slidesPerView: 3,
    };
  };

  return (
    <PublicationWrapper className="section publication" id={`section-${id}`}>
      <ContentSection>
        <PageHeader title="Publication" />
        <div className="component_body">
          <div className="component_slide_handler left" ref={prevSlideBtn}>
            <IconArrowDownCircle />
          </div>
          <div
            aria-hidden="true"
            className="component_slide_handler right"
            ref={nextSlideBtn}
          >
            <IconArrowDownCircle />
          </div>
          <div className="component_card_list">
            <Swiper
              {...onGetSwiperHandler()}
              loop={true}
              slidesPerGroup={1}
              spaceBetween={24}
              nextOnClick={nextSlideBtn}
              prevOnClick={prevSlideBtn}
              dataSource={dummy}
              renderItem={(item, index) => {
                return (
                  <div key={index} className="component_card_list_item">
                    <div className="component_item_image">
                      {item?.image_url && (
                        <img src={item?.image_url} alt="card image" />
                      )}
                      {!item?.image_url && <IconFileImage />}
                    </div>
                    <div className="component_item_text">
                      <div className="component_date">
                        <IconCalender />
                        {item?.created_at}
                      </div>
                      <div className="component_title">{item?.title}</div>
                      <div className="component_desc">{item?.desc}</div>
                    </div>
                  </div>
                );
              }}
            />
          </div>
        </div>
        <div className="component_cta">
          <Button
            type="primary"
            className="transparent"
            onClick={() => {
              router.push(BASE_URL.PUBLICATION);
            }}
          >
            See More
          </Button>
        </div>
      </ContentSection>
    </PublicationWrapper>
  );
};

export default PublicationSection;
