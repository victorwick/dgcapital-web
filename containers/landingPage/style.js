import styled from "styled-components";
import breakpoints from "../../configs/breakpoints";
import colors from "../../configs/colors";

const Wrapper = styled.div`
  .section {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: 100vh;
  }
  @media screen and (max-width: ${breakpoints.tab}) {
    .section {
      min-height: 720px;
    }
  }
  @media screen and (max-height: 720px) {
    .section {
      min-height: 720px;
    }
  }
`;

export default Wrapper;

export const MainWrapper = styled.div`
  background: ${colors.gradientCoal};
  position: relative;
  .container_main {
    max-width: calc(1128px + 40px);
    padding: 0 40px;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    .component_main_text {
      display: grid;
      flex-wrap: wrap;
      gap: 40px;
      flex: 1;
      .component_header {
        font-size: 48px;
        font-weight: 700;
        max-width: 560px;
        line-height: 56px;
        color: ${colors.white900};
      }
      .component_divider {
        width: 72px;
        height: 1px;
        background-color: ${colors.white900};
      }
      .component_description {
        font-size: 20px;
        font-weight: 500;
        line-height: 28px;
        color: ${colors.teal500};
        max-width: 530px;
      }
    }

    .component_image {
      flex: 1;
      max-width: 456px;
    }
  }
  .component_main_bg {
    position: absolute;
    width: 100%;
    bottom: 0;
    left: 0;
    .component_main_bg_wrapper {
      position: relative;
      .component_scroll_down {
        max-width: calc(1128px + 40px);
        padding: 0 40px;
        position: absolute;
        top: 50%;
        width: 100%;
        left: 50%;
        transform: translate(-50%, -50%);
        display: flex;
        .component_scroll_down_btn {
          display: flex;
          align-items: center;
          gap: 16px;
          cursor: pointer;
          color: ${colors.white900};
          font-size: 12px;
          font-weight: 600;
          transition: 400ms ease-in-out;
          :hover {
            transform: translateY(8px);
          }
        }
      }
    }
  }

  @media screen and (max-width: ${breakpoints.tab_large}) {
    .container_main {
      display: grid;
      grid-template-columns: auto auto;
      .component_main_text {
        gap: 20px;
        .component_header {
          font-size: 32px;
        }
        .component_description {
          font-size: 16px;
        }
      }
    }
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    .container_main {
      .component_main_text {
        .component_header {
          font-size: 24px;
          line-height: unset;
        }
        .component_description {
          font-size: 14px;
          line-height: unset;
        }
      }
    }
  }

  @media screen and (max-width: ${breakpoints.phone}) {
    .container_main {
      margin-top: 40px;
      margin-bottom: 20px;
      grid-template-columns: 1fr;
      .component_image {
        max-width: 240px;
        margin: auto;
      }
    }
    .component_main_bg {
      .component_main_bg_wrapper {
        .component_bg {
          padding-top: 40px;
        }
        .component_scroll_down {
          .component_scroll_down_btn {
            display: flex;
            align-items: center;
            gap: 8px;
            cursor: pointer;
            color: ${colors.white900};
            font-size: 12px;
            font-weight: 600;
            transition: 400ms ease-in-out;
            :hover {
              transform: translateY(8px);
            }
            svg {
              transform: scale(0.8);
            }
          }
        }
      }
    }
  }
`;

export const AboutWrapper = styled.div`
  background: ${colors.gray500};
  position: relative;
  .component_body {
    display: grid;
    grid-template-columns: auto auto;
    justify-content: end;
    gap: 40px;
    .component_body_text {
      max-width: 500px;
      .component_title {
        font-size: 36px;
        font-weight: 600;
        color: ${colors.gray900};
        margin-bottom: 24px;
      }
      .component_description {
        font-size: 16px;
        font-weight: 400;
        line-height: 24px;
        color: ${colors.gray600};
      }
    }
    .component_body_image {
      max-width: 456px;
    }
  }

  .component_about_bg {
    position: absolute;
    left: 24px;
    bottom: 24px;
    width: 16vw;
  }

  @media screen and (max-width: ${breakpoints.tab_large}) {
    .component_body {
      gap: 20px;
      .component_body_text {
        .component_title {
          font-size: 32px;
        }
      }
    }
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    .component_body {
      gap: 40px;
      .component_body_text {
        .component_title {
          font-size: 24px;
          margin-bottom: 24px;
        }
        .component_description {
          font-size: 14px;
          line-height: 20px;
        }
      }
    }
  }
  @media screen and (max-width: ${breakpoints.phone}) {
    .component_body {
      grid-template-columns: 1fr;
      .component_body_image {
        max-width: 240px;
        margin: auto;
      }
    }
  }
`;

export const PortfolioWrapper = styled.div`
  background: ${colors.gray100};
  position: relative;
  .component_body {
    max-width: 744px;
    margin: 0 auto;
    display: flex;
    flex-wrap: wrap;
    gap: 40px;
    z-index: 2;
    .component_description {
      font-size: 16px;
      font-weight: 400;
      line-height: 24px;
      color: ${colors.gray600};
    }
    .component_card_list {
      display: flex;
      gap: 40px;
      justify-content: space-between;
      align-items: flex-start;
      z-index: 2;
      .component_card_list_item {
        cursor: pointer;
        .component_card_image_wrapper {
          box-shadow: 0px 1px 2px 0px #0000000f;
          box-shadow: 0px 1px 3px 0px #0000001a;
          width: 156px;
          height: 156px;
          overflow: hidden;
          background-color: ${colors.white900};
          display: flex;
          justify-content: center;
          align-items: center;
          position: relative;
          img {
            width: 100%;
            height: 100%;
            object-fit: cover;
          }
          svg {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
          }
        }
        .component_card_text_wrapper {
          margin-top: 16px;
          text-align: center;
          font-size: 16px;
          font-weight: 600;
        }
      }
    }
  }

  .component_cta {
    margin-top: 48px;
    display: flex;
    align-items: center;
    justify-content: end;
    max-width: 940px;
    margin-left: auto;
    margin-right: auto;
    > button {
      z-index: 2;
    }
  }
  .component_about_bg {
    position: absolute;
    z-index: 1;
    &.left {
      left: 24px;
      bottom: 24px;
    }
    &.right {
      right: 0;
      bottom: 0;
    }
  }
  @media screen and (max-width: ${breakpoints.tab_large}) {
    padding: 12vw 0;
    .component_body {
      flex-wrap: wrap;
      .component_card_list {
        flex-wrap: wrap;
        justify-content: space-around;
      }
    }
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    .component_body {
      gap: 20px;
      .component_description {
        font-size: 14px;
        line-height: 20px;
      }
    }
  }
  @media screen and (max-width: ${breakpoints.phone}) {
    .component_body {
      gap: 20px;
      .component_description {
        font-size: 14px;
        line-height: 24px;
      }
      .component_card_list {
        .component_card_list_item {
          width: 100%;
          .component_card_image_wrapper {
            position: relative;
            overflow: hidden;
            padding-bottom: 100%;
            width: 100%;
            img {
              position: absolute;
              top: 0;
              left: 0;
            }
          }
        }
      }
    }
    .component_cta {
      > button {
        width: 100%;
      }
    }
  }
`;

export const PublicationWrapper = styled.div`
  background: ${colors.gray200};
  .component_body {
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    .component_slide_handler {
      position: absolute;
      &.left {
        left: 0;
        transform: rotate(90deg);
        transform-origin: center;
      }
      &.right {
        right: 0;
        transform: rotate(270deg);
        transform-origin: center;
      }
      cursor: pointer;
      path {
        fill: #9d9d9d;
        transition: 0.3s ease-in-out;
      }
      :hover path {
        fill: ${colors.teal500};
      }
    }
    .component_card_list {
      max-width: 86.5%;
      margin: auto;
      .component_card_list_item {
        background: ${colors.white900};
        padding: 16px;
        display: grid;
        row-gap: 12px;
        .component_item_image {
          width: 100%;
          height: 156px;
          overflow: hidden;
          background: ${colors.gray200};
          position: relative;
          img {
            width: 100%;
            height: 100%;
            object-fit: cover;
          }
          svg {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
          }
        }
        .component_item_text {
          display: grid;
          row-gap: 12px;
          .component_date {
            display: flex;
            align-items: center;
            gap: 8px;
            font-size: 12px;
            font-weight: 400;
            color: ${colors.gray600};
          }
          .component_title {
            font-size: 20px;
            font-weight: 700;
            color: ${colors.gray900};
            line-height: 28px;
          }
          .component_desc {
            font-size: 14px;
            font-weight: 400;
            line-height: 20px;
          }
        }
      }
    }
  }

  .component_cta {
    max-width: 86.5%;
    margin: 0 auto;
    display: flex;
    justify-content: end;
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    .component_body {
      padding-bottom: 28px;
      .component_slide_handler {
        bottom: 0;
        &.left {
          right: 72px;
          left: unset;
        }
        &.right {
          right: 0;
        }
      }
      .component_card_list {
        max-width: 100%;
        .component_card_list_item {
          .component_item_text {
            display: grid;
            row-gap: 12px;
            .component_title {
              font-size: 16px;
              line-height: 24px;
            }
            .component_desc {
              font-size: 12px;
              line-height: 18px;
            }
          }
        }
      }
    }
    .component_cta {
      margin-top: 24px;
      max-width: 100%;
    }
  }

  @media screen and (max-width: ${breakpoints.phone}) {
    .component_body {
      padding-bottom: 28px;
      .component_slide_handler {
        max-width: fit-content;
        &.left {
          right: unset;
          left: calc(50% - 50px);
        }
        &.right {
          right: calc(50% - 50px);
          left: unset;
        }
      }
    }
    .component_cta {
      margin-top: 24px;
      max-width: 100%;
      button {
        width: 100%;
      }
    }
  }
`;

export const ContactWrapper = styled.div`
  background: ${colors.gradientCoal};
  .component_body {
    max-width: 940px;
    margin: 0 auto;
    display: grid;
    grid-template-columns: auto 50%;
    column-gap: 24px;
    row-gap: 60px;
    .component_body_text {
      .component_title {
        font-size: 20px;
        font-weight: 600;
        line-height: 28px;
        color: ${colors.white900};
      }
      .component_desc {
        font-size: 18px;
        font-weight: 500;
        line-height: 28px;
        color: ${colors.white900};
        margin-top: 16px;
        margin-bottom: 24px;
      }
      .component_map {
        height: 304px;
        display: flex;
        position: relative;
        ::before {
          content: "";
          position: absolute;
          background-color: ${colors.teal500};
          width: calc(100% - 22px);
          height: calc(100% - 16px);
          z-index: 1;
        }
        .component_iframe {
          display: grid;
          width: 100%;
          height: 100%;
          align-items: end;
          justify-items: end;
          z-index: 2;
          iframe {
            width: calc(100% - 22px);
            height: calc(100% - 16px);
          }
        }
      }
    }
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    padding: 40px 0 60px 0;
    .component_body {
      grid-template-columns: auto;
      display: flex;
      flex-direction: column;
      .component_body_text {
        .component_title {
          font-size: 20px;
          font-weight: 600;
          line-height: 28px;
          color: ${colors.white900};
        }
        .component_desc {
          font-size: 18px;
          font-weight: 500;
          line-height: 28px;
          color: ${colors.white900};
          margin-top: 16px;
          margin-bottom: 24px;
        }
      }
    }
  }
`;
