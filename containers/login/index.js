import { Checkbox, Input } from "antd";
import Image from "next/image";
import React from "react";
import {
  IconDGVLogoWhite,
  ImageBlockchainSecondary,
  ImageDotsPattern,
  ImageLineRectanglePattern,
} from "../../assets";
import { Button, Form } from "../../components";
import Wrapper from "./style";

const Index = () => {
  return (
    <Wrapper>
      <div className="container_login">
        <div className="component_base">
          <div className="component_image">
            <Image src={ImageBlockchainSecondary} alt="image base" />
          </div>
          <div className="component_text">
            <div className="component_title">
              <span>Lorem Ipsum </span>
              <span className="component_title_highlight">Dolor Sit Amet</span>
            </div>
            <div className="component_desc">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu
              aliquam massa, in.
            </div>
          </div>
          <div className="component_bg_line">
            <Image src={ImageLineRectanglePattern} alt="" />
          </div>
          <div className="component_bg_dots">
            <Image src={ImageDotsPattern} alt="" />
          </div>
        </div>
        <div className="component_form">
          <div className="component_logo">
            <Image src={IconDGVLogoWhite} alt="" />
          </div>
          <div className="component_header">
            <div className="component_title">Login</div>
            <div className="component_desc">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </div>
          </div>
          <Form layout="vertical" theme="dark" rounded>
            <Form.Item
              label="Email"
              required
              rules={[
                {
                  required: true,
                },
              ]}
              name="email"
            >
              <Input placeholder="mail@website.com" />
            </Form.Item>
            <Form.Item
              label="Password"
              required
              name="password"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input.Password placeholder="Min. 8 Character" />
            </Form.Item>
            <Form.Item name="remember">
              <Checkbox>Remember me</Checkbox>
            </Form.Item>
            <Button type="primary" block rounded>
              Login
            </Button>
          </Form>
        </div>
      </div>
    </Wrapper>
  );
};

export default Index;
