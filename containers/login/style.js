import styled from "styled-components";
import breakpoints from "../../configs/breakpoints";
import colors from "../../configs/colors";

const Wrapper = styled.div`
  min-height: 100vh;
  display: grid;
  background: ${colors.gradientCoal};
  min-width: ${breakpoints.tab_large};
  .container_login {
    display: grid;
    grid-template-columns: 50% 50%;
    overflow: auto;
    .component_base {
      background: ${colors.gradientCoal};
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      position: relative;
      gap: 24px;
      overflow: hidden;
      padding: 80px 40px;
      .component_text {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        gap: 16px;
        max-width: 476px;
        text-align: center;
        color: ${colors.white900};
        .component_title {
          font-size: 24px;
          font-weight: 700;
          line-height: 32px;
          .component_title_highlight {
            color: ${colors.teal500};
          }
        }
        .component_desc {
          font-size: 16px;
          font-weight: 500;
          line-height: 24px;
        }
      }

      .component_bg_line {
        position: absolute;
        right: 0;
        top: 0;
      }
      .component_bg_dots {
        position: absolute;
        left: 24px;
        bottom: -54px;
      }
    }
    .component_form {
      display: flex;
      align-items: flex-start;
      justify-content: center;
      flex-direction: column;
      max-width: 480px;
      margin: auto;
      width: 100%;
      gap: 40px;
      padding: 80px 40px;
      .component_header {
        .component_title {
          font-size: 24px;
          font-weight: 700;
          line-height: 32px;
          color: ${colors.white900};
          margin-bottom: 8px;
        }
        .component_desc {
          font-size: 16px;
          font-weight: 500;
          line-height: 24px;
          color: ${colors.gray400};
        }
      }
    }
  }
`;

export default Wrapper;
