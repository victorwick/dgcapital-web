import { useRouter } from "next/router";
import React, { useState } from "react";
import { IconClose, IconFileImage, IconGlobe } from "../../assets";
import {
  Header,
  PageHeader,
  ContentSection,
  Button,
  Footer,
} from "../../components";
import BASE_URL from "../../configs/baseUrl";
import Wrapper, { DetailPortfolioWrapper } from "./style";

const dummy = [
  {
    id: "1",
    name: "Card name 1",
    image_url:
      "https://images.pexels.com/photos/1622419/pexels-photo-1622419.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
  {
    id: "2",
    name: "Card name 2",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
  {
    id: "3",
    name: "Card name 3",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
  {
    id: "4",
    name: "Card name 4",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
  {
    id: "5",
    name: "Card name 1",
    image_url:
      "https://images.pexels.com/photos/1622419/pexels-photo-1622419.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
  {
    id: "6",
    name: "Card name 2",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
  {
    id: "7",
    name: "Card name 3",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
  {
    id: "8",
    name: "Card name 4",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
  {
    id: "9",
    name: "Card name 1",
    image_url:
      "https://images.pexels.com/photos/1622419/pexels-photo-1622419.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
  {
    id: "10",
    name: "Card name 2",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    extra:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut faucibus adipiscing consequat neque vel ultrices quis arcu, nec. Fusce urna lacus vitae, tellus volutpat. Ultrices iaculis euismod viverra rhoncus at cras malesuada.",
  },
];

const Index = () => {
  const currentPage = 2;
  const router = useRouter();
  const [showDetail, setShowDetail] = useState(false);

  const onChangePageHandler = (page) => {
    router.push({
      pathname: BASE_URL.LANDING,
      query: { active_section: page },
    });
  };

  const onCloseDetail = () => {
    return setShowDetail(false);
  };

  return (
    <Wrapper>
      <Header
        currentPage={currentPage}
        onChangePageHandler={onChangePageHandler}
      />
      <DetailPortfolioWrapper
        visible={showDetail}
        closable={false}
        placement="bottom"
        onClose={onCloseDetail}
        height="auto"
      >
        <div className="component_header">
          <PageHeader title="Our Portfolio" margin_top={42} full_width />
          <div
            aria-hidden="true"
            className="component_header_close"
            onClick={onCloseDetail}
          >
            <IconClose />
          </div>
        </div>
        <div className="component_body">
          <div className="component_body_text">
            <div className="component_title">{showDetail?.name}</div>
            <div className="component_desc">{showDetail?.desc}</div>
            <div className="component_extra">{showDetail?.extra}</div>
            <div className="component_link">
              <IconGlobe />
              https://www.qoala.app/id/blog/tentang-kami/
            </div>
          </div>
          <div className="component_body_image">
            {showDetail?.image_url && (
              <img src={showDetail?.image_url} alt="card image" />
            )}
            {!showDetail?.image_url && <IconFileImage />}
          </div>
        </div>
      </DetailPortfolioWrapper>
      <ContentSection>
        <PageHeader title="Our Portfolio" margin_top={120} />
        <div className="component_body">
          <div className="component_card_list">
            {dummy?.map((item) => {
              return (
                <div key={item?.id} className="component_card_list_item">
                  <div className="component_card_image_wrapper">
                    {item?.image_url && (
                      <img src={item?.image_url} alt="card image" />
                    )}
                    {!item?.image_url && <IconFileImage />}
                  </div>
                  <div className="component_card_text_wrapper">
                    <div className="component_name">{item?.name}</div>
                    <div className="component_desc">{item?.desc}</div>
                    <div className="component_cta">
                      <Button
                        type="secondary"
                        size="small"
                        className="transparent"
                        onClick={() => {
                          setShowDetail(item);
                        }}
                      >
                        See More
                      </Button>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </ContentSection>
      <Footer />
    </Wrapper>
  );
};

export default Index;
