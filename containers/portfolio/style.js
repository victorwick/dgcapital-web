import { Drawer } from "antd";
import styled from "styled-components";
import breakpoints from "../../configs/breakpoints";
import colors from "../../configs/colors";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  justify-content: center;
  background: ${colors.gray100};
  min-height: 100vh;
  .component_body {
    max-width: 744px;
    margin: 0 auto;
    .component_card_list {
      display: flex;
      gap: 24px;
      justify-content: center;
      align-items: flex-start;
      flex-wrap: wrap;
      .component_card_list_item {
        cursor: pointer;
        position: relative;
        overflow: hidden;
        .component_card_image_wrapper {
          width: 232px;
          height: 232px;
          box-shadow: 0px 1px 2px 0px #0000000f;
          box-shadow: 0px 1px 3px 0px #0000001a;
          overflow: hidden;
          background-color: ${colors.white900};
          display: flex;
          justify-content: center;
          align-items: center;
          position: relative;
          z-index: 1;
          img {
            width: 100%;
            height: 100%;
            object-fit: cover;
          }
          svg {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
          }
        }

        .component_card_text_wrapper {
          visibility: hidden;
          position: absolute;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          background: rgba(13, 148, 136, 0.72);
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          gap: 24px;
          padding: 24px;
          color: ${colors.white900};
          z-index: 2;
          transform: translateX(100%);
          .component_name {
            font-size: 24px;
            font-weight: 32px;
            line-height: 32px;
            text-align: center;
          }
          .component_desc {
            font-size: 12px;
            font-weight: 400;
            line-height: 20px;
            text-align: center;
          }
        }
        :hover {
          .component_card_text_wrapper {
            visibility: visible;
            transform: translateX(0);
            transition: all 0.3s ease-out;
          }
        }
      }
    }
  }

  .component_detail_portfolio {
    background-color: red;
  }

  @media screen and (max-width: ${breakpoints.phone}) {
    .component_body {
      .component_card_list {
        flex-wrap: nowrap;
        flex-direction: column;
        .component_card_list_item {
          width: 100%;
          height: 242px;
          .component_card_image_wrapper {
            width: 100%;
            height: 100%;
          }
          .component_card_text_wrapper {
            visibility: visible;
            transform: translateX(0);
          }
        }
      }
    }
  }
`;

export default Wrapper;

export const DetailPortfolioWrapper = styled(Drawer)`
  .ant-drawer-content {
    background-color: #f5f5f4;
    padding: 0 40px 80px 40px;
    .ant-drawer-body {
      max-width: calc(1128px);
      margin: auto;
      width: 100%;
      padding: 0;
    }
  }
  .component_header {
    display: flex;
    align-items: center;
    justify-content: space-between;
    .component_header_close {
      cursor: pointer;
      svg {
        fill: ${colors.gray600};
      }
      :hover svg {
        fill: ${colors.gray900};
      }
    }
  }

  .component_body {
    display: flex;
    justify-content: space-between;
    gap: 24px;
    .component_body_text {
      color: #525252;
      max-width: 744px;
      flex: 1;
      .component_title {
        font-size: 30px;
        font-weight: 600;
        line-height: 36px;
        color: ${colors.teal500};
        margin-bottom: 16px;
      }
      .component_desc {
        font-size: 20px;
        font-weight: 400;
        line-height: 28px;
        margin-bottom: 24px;
      }
      .component_extra {
        font-size: 14px;
        font-weight: 400;
        line-height: 20px;
        margin-bottom: 40px;
      }
      .component_link {
        display: flex;
        align-items: center;
        gap: 8px;
        font-size: 14px;
        font-weight: 600;
        line-height: 20px;
        cursor: pointer;
      }
    }
    .component_body_image {
      min-width: 232px;
      background-color: ${colors.white900};
      width: 232px;
      height: 232px;
      overflow: hidden;
      position: relative;
      box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.06);
      box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.1);
      img {
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
      svg {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
      }
    }
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    .ant-drawer-content {
      background-color: #f5f5f4;
      padding: 0 40px 80px 40px;
      .ant-drawer-body {
        max-width: calc(1128px);
        margin: auto;
        width: 100%;
        padding: 0;
      }
    }
    .component_header {
      display: flex;
      align-items: center;
      justify-content: space-between;
      .component_header_close {
        cursor: pointer;
        svg {
          fill: ${colors.gray600};
        }
        :hover svg {
          fill: ${colors.gray900};
        }
      }
    }

    .component_body {
      flex-direction: column-reverse;
      .component_body_image {
        width: 100%;
      }
    }
  }
`;
