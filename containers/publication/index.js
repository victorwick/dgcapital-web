import React from "react";
import { useRouter } from "next/router";
import { ContentSection, Footer, Header, PageHeader } from "../../components";
import Wrapper from "./style";
import { IconCalender, IconFileImage } from "../../assets";
import BASE_URL from "../../configs/baseUrl";

const dummy = [
  {
    id: "1",
    title: "#1 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  },
  {
    id: "2",
    title: "#2 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
  },
  {
    id: "3",
    title: "#3 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
  },
  {
    id: "4",
    title: "#4 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
  },
  {
    id: "5",
    title: "#5 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  },
  {
    id: "6",
    title: "#6 Tips Supaya Sukses Berinvestasi Diusia Muda",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Imperdiet rhoncus in quis etiam consequat tellus...",
    created_at: "Min, 12 Des 2021",
    image_url:
      "https://images.pexels.com/photos/1165509/pexels-photo-1165509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  },
];

const Index = () => {
  const currentPage = 3;
  const router = useRouter();

  const onChangePageHandler = (page) => {
    router.push({
      pathname: BASE_URL.LANDING,
      query: { active_section: page },
    });
  };

  return (
    <Wrapper>
      <Header
        currentPage={currentPage}
        onChangePageHandler={onChangePageHandler}
      />
      <ContentSection>
        <PageHeader title="All Publication" margin_top={120} />
        <div className="component_body">
          <div className="component_card_list">
            {dummy.map((item) => {
              return (
                <div
                  key={item?.id}
                  className="component_card_list_item"
                  onClick={() => {
                    router.push(`${BASE_URL.PUBLICATION}/${item?.id}`);
                  }}
                >
                  <div className="component_item_image">
                    {item?.image_url && (
                      <img src={item?.image_url} alt="card image" />
                    )}
                    {!item?.image_url && <IconFileImage />}
                  </div>
                  <div className="component_item_text">
                    <div className="component_date">
                      <IconCalender />
                      {item?.created_at}
                    </div>
                    <div className="component_title">{item?.title}</div>
                    <div className="component_desc">{item?.desc}</div>
                    <div className="component_read_more">Read more {">"}</div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </ContentSection>
      <Footer />
    </Wrapper>
  );
};

export default Index;
