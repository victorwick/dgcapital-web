import styled from "styled-components";
import breakpoints from "../../configs/breakpoints";
import colors from "../../configs/colors";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  background: ${colors.gray100};
  min-height: 100vh;
  .component_body {
    max-width: 744px;
    margin: 0 auto;
    .component_card_list {
      display: flex;
      flex-direction: column;
      gap: 32px;
      .component_card_list_item {
        display: grid;
        grid-template-columns: 1fr 1fr;
        gap: 32px;
        .component_item_image {
          height: 180px;
          width: 100%;
          overflow: hidden;
          background: #c4c4c4;
          position: relative;
          img {
            width: 100%;
            height: 100%;
            object-fit: cover;
          }
          svg {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
          }
        }
        .component_item_text {
          display: flex;
          flex-direction: column;
          gap: 16px;
          .component_date {
            display: flex;
            align-items: center;
            gap: 8px;
            font-size: 12px;
            font-weight: 400;
            line-height: 16px;
            color: ${colors.gray600};
          }
          .component_title {
            font-size: 20px;
            font-weight: 700;
            line-height: 28px;
            color: ${colors.gray900};
          }
          .component_desc {
            font-size: 14px;
            font-weight: 400;
            line-height: 20px;
            color: ${colors.gray600};
          }
          .component_read_more {
            font-size: 14px;
            font-weight: 400;
            line-height: 20px;
            color: ${colors.teal500};
            cursor: pointer;
          }
        }
      }
    }
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    .component_body {
      .component_card_list {
        .component_card_list_item {
          grid-template-columns: 1fr;
          gap: 16px;
          .component_item_image {
            height: 240px;
          }
          .component_item_text {
            gap: 12px;
          }
        }
      }
    }
  }
`;

export default Wrapper;
