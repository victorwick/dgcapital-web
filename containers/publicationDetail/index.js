import React from "react";
import { useRouter } from "next/router";
import { ContentSection, Footer, Header, PageHeader } from "../../components";
import Wrapper from "./style";
import BASE_URL from "../../configs/baseUrl";
import { IconCalender, IconFileImage } from "../../assets";
import dummy from "../../configs/dummy";

const Index = () => {
  const currentPage = 3;
  const router = useRouter();
  const publicationId = router?.query?.id;

  const onChangePageHandler = (page) => {
    router.push({
      pathname: BASE_URL.LANDING,
      query: { active_section: page },
    });
  };

  const onGetPublication = dummy?.publication?.find((item) => {
    return item?.id === publicationId;
  });

  return (
    <Wrapper>
      <Header
        currentPage={currentPage}
        onChangePageHandler={onChangePageHandler}
      />
      <ContentSection>
        <PageHeader title="Publication" margin_top={120} />
        <div className="component_body">
          <div className="component_title">{onGetPublication?.title}</div>
          <div className="component_date">
            <IconCalender />
            {onGetPublication?.created_at}
          </div>
          <div className="component_image">
            {onGetPublication?.image_url && (
              <img src={onGetPublication?.image_url} alt="card image" />
            )}
            {!onGetPublication?.image_url && <IconFileImage />}
          </div>
          <div className="component_desc">{onGetPublication?.desc}</div>
        </div>
      </ContentSection>
      <Footer />
    </Wrapper>
  );
};

export default Index;
