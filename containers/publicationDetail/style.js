import styled from "styled-components";
import breakpoints from "../../configs/breakpoints";
import colors from "../../configs/colors";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  background: ${colors.gray100};
  min-height: 100vh;
  .component_body {
    max-width: 744px;
    margin: 0 auto;
    .component_title {
      font-size: 30px;
      font-weight: 600;
      line-height: 36px;
      color: ${colors.gray900};
      margin-bottom: 16px;
    }
    .component_date {
      display: flex;
      align-items: center;
      gap: 8px;
      font-size: 14px;
      font-weight: 500;
      line-height: 20px;
      color: ${colors.gray600};
      margin-bottom: 24px;
    }
    .component_image {
      height: 393px;
      width: 100%;
      overflow: hidden;
      background: #c4c4c4;
      position: relative;
      margin-bottom: 40px;
      img {
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
      svg {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) scale(1.25);
        transform-origin: center;
      }
    }
    .component_desc {
      font-size: 20px;
      font-weight: 400;
      line-height: 28px;
      color: ${colors.gray600};
      white-space: pre-wrap;
    }
  }

  @media screen and (max-width: ${breakpoints.tab}) {
    .component_body {
      .component_title {
        font-size: 24px;
      }
      .component_image {
        height: 280px;
      }
      .component_desc {
        font-size: 16px;
      }
    }
  }
`;

export default Wrapper;
