const withAntdLess = require("next-plugin-antd-less");

module.exports = {
  images: {
    domains: ["images.pexels.com"],
  },
  reactStrictMode: true,
  compress: false,
};

module.exports = withAntdLess({
  modifyVars: { "@primary-color": "#14b8a6" },
  cssLoaderOptions: {},
  webpack(config) {
    return config;
  },
});
